import {post} from "@/utils/request";

export function login(data, ok, fail) {
    post('/admin/login', data, ok, fail)
}

export function logout(ok, fail) {
    post('/admin/logout', null, ok, fail)
}

export function getInfo(ok, fail) {
    post('/admin/getInfo', null, ok, fail)
}

export function getRouters(roles, ok) {
    post('/admin/getRouters', {roleIds: roles}, ok)
}


export function getUserRoleAndPost(ok) {
    post('/admin/getUserRoleAndPost', null, ok)
}

export function update(data, ok) {
    post('/admin/update', data, ok)
}

export function setPassword(password, newPassword, ok) {
    post('/admin/setPassword', {password: password, newPassword: newPassword}, ok)
}